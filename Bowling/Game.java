package lab03;
import java.util.ArrayList;

public class Game {
	
	int[] rolls;
	int currentRoll;
	
	public Game(){
		this.rolls = new int[21];
		currentRoll = 0;
	}
	
	public void roll(int pins) {
		rolls[currentRoll++] = pins;	
	}

	public int score() {	
		int score = 0;
		int rollPoint = 0; // indica i punti a ogni lancio
		for (int i = 0; i < 10; i++) {// scorre tutti i frame
			if (isStrike(rollPoint)) {
				score += 10 + strikeBonus(rollPoint);
				rollPoint++;
			} else if (isSpare(rollPoint)) {
				score += 10 + spareBonus(rollPoint);
				rollPoint += 2;
			} else {
				score += sumOfRolls(rollPoint);
				rollPoint += 2;
			}
		}

		return score;
	}
	
	private boolean isStrike(int rollPoint) {
		return rolls[rollPoint] == 10;
	}

	private boolean isSpare(int rollPoint) {
		return sumOfRolls(rollPoint) == 10;
	}

	private int strikeBonus(int rollPoint) {
		return sumOfRolls(rollPoint+1);
	}

	private int spareBonus(int rollPoint) {
		return rolls[rollPoint+2];
	}

	private int sumOfRolls(int rollPoint) {
		return rolls[rollPoint] + rolls[rollPoint+1];
	}
	

}
