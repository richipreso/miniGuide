package lab03;

import org.slf4j.*;
public class Main {
	public static Game G1;

	public static void main(String[] args) {
		Logger log = LoggerFactory.getLogger(Main.class);
		// TODO Auto-generated method stub
		G1 = new Game();
		G1.roll(10);
		G1.roll(10);
		G1.roll(10);
		G1.roll(7);
		G1.roll(2);
		log.info("Primo parziale {}",G1.score());
		G1.roll(8);
		G1.roll(2);
		G1.roll(0);
		G1.roll(9);
		G1.roll(10);
		G1.roll(7);
		G1.roll(3);
		G1.roll(9);
		G1.roll(0);
		log.info("Secondo parziale {}",G1.score());
		G1.roll(10);
		G1.roll(10);
		G1.roll(8);
		log.info("Risultato finale {}",G1.score());
		
	}

}
