note
	description: "GAME application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature -- Access
	G:GAME

feature {NONE} -- Initialization
	make
		-- Run application
		do

			create G.make
		 	G.add_roll(10)
		 	G.add_roll(10)
		 	G.add_roll(10)
		 	G.add_roll(7)
		 	G.add_roll(2)
		 	across G.rolls
		 		 as roll
		 	loop
		 		print(roll.item.out +"%N")
		 	end
		 	print ("parziale: "+ G.score.out +"%N")
		end

end
