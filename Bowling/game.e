note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature	--inizializzation
	make
		do
			set_current_roll(0)
			create rolls.make_empty
		end

feature	--status
	current_roll: INTEGER
	rolls: ARRAY[INTEGER]

feature --settings
	set_current_roll(n:INTEGER)
		require
			current_roll_exist: n >= 0 and n <= 20
		do
			current_roll := n
		ensure
			current_roll_set: current_roll = n
		end

	add_roll(n:INTEGER)
		require
			adding_roll_exist: n >= 0 and n <= 10
		do
			rolls.force(n,rolls.count+1)
		ensure
			roll_added: rolls.has(n)
			one_more_roll: rolls.count = old rolls.count+1
		end

	is_strike(roll_point:INTEGER): BOOLEAN
	require
		valid_roll: roll_point >= 0 and roll_point <= 20
	do
		Result := (rolls.item (roll_point) = 10)
	end

	is_spare(roll_point:INTEGER):BOOLEAN
		require
			valid_roll: roll_point >= 0 and roll_point <= 20
		do
			Result := sum_of_rolls(roll_point) = 10
		end

	strike_bonus(roll_point:INTEGER):INTEGER
		require
			valid_roll: roll_point >= 0 and roll_point <= 20
		do
			Result := sum_of_rolls(roll_point+1)
		ensure
			valid_result_strike: Result >=0 and Result <= 20
		end

	spare_bonus(roll_point:INTEGER):INTEGER
		require
			valid_roll: roll_point >= 0 and roll_point <= 19
		do
			Result := rolls.item (roll_point+2)
		ensure
			valid_result_spare:Result >= 0 and Result <= 10
		end

	sum_of_rolls(roll_point:INTEGER):INTEGER
		require
			valid_roll: roll_point >= 0 and roll_point <= 20
		do
			Result := rolls.item (roll_point)+ rolls.item(roll_point+1)
		ensure
			sum_is_valid: rolls.item (roll_point)+ rolls.item (roll_point+1) >= 0 and rolls.item (roll_point)+ rolls.item (roll_point+1) <= 20
		end

	score:INTEGER
	-- mettere i require
		local
			-- gli INTEGER inizializzano automaticamente a zero(o almeno co� ho capito)
			s:INTEGER -- risultato parziale
			roll_point: INTEGER
			i: INTEGER
		do
			from
				i := 0
			until
				i < 10
			loop

				if is_strike(roll_point) then
					s := s + 10 + strike_bonus(roll_point)
					roll_point := roll_point +1
				elseif is_spare(roll_point) then
					s:= s + 10 + spare_bonus(roll_point)
					roll_point := roll_point +2
				else
					s:= s+ sum_of_rolls(roll_point)
					roll_point := roll_point +2
				end
				i := i+1
			end

			Result := s
		end
		-- mettere gli ensure

invariant
valid_current_roll : current_roll >= 0 and current_roll <= 20
end
